import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner in = new Scanner(System.in);
        /* Enter integer values A and B from the keyboard */
        System.out.println("Enter value A:");
        int a = in.nextInt();
        System.out.println("Enter value B (B > 0):");
        int b = in.nextInt();

        calculator.addition(a, b);
        calculator.subtraction(a, b);
        calculator.multiplication(a, b);
        calculator.division(a, b);
    }

    public void addition(int a, int b) {
        int sum = a + b;
        System.out.println("Sum of values = " + sum);
    }

    public void subtraction(int a, int b) {
        int subtract = a - b;
        System.out.println("Difference of values = " + subtract);
    }

    public void multiplication(int a, int b) {
        int multi = a * b;
        System.out.println("Multiplying values = " + multi);
    }

    public void division(int a, int b) {
        if (b == 0) {
            System.out.println("B = 0 - division is not impossible");
        }
        else {
            int div = a / b;
            System.out.println("Division of values = " + div);
        }
    }
}
